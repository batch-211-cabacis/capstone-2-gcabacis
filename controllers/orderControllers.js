const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");
const User = require("../models/User");



// Create an order

module.exports.createOrder =  async (data) =>{
		
		
		let productOrder =  await Product.findById(data.productId).then(product=>{
			product.products.push({
				orderId: data.orderId,
				quantity: data.quantity
			})	
			return product.save().then((product,error)=>{
				if(error){
					return false
				}else{
					return true
				}
			})
		});
		let userOrder =  await User.findById(data.userId).then(user=>{
			user.products.push({
				orderId: data.orderId,
				quantity: data.quantity
			})	
			return user.save().then((user,error)=>{
				if(error){
					return false;
				}else{
					return true
				}
			})
		});
		if(productOrder && userOrder){

			let newOrder = await Order({
		totalAmount: data.totalAmount,
		userId: data.userId,
		products: [{
			productId: data.productId,
			quantity: data.quantity
			}]
			});

		return newOrder.save().then((order,error)=>{
		order.products.push({productId:data.productId})
		if(error){
			return false;
		}else{
			return "Order successful!";
		}
	});
	}
};


// Retrive a single order 

module.exports.getOrder = (reqParams) =>{
	return Order.findById(reqParams.orderId).then(result=>{
		return result;
	})
};


// Retrieve orders

module.exports.getAllOrder = () =>{
	return Order.find({}).then(result=>{
		return result;
	})
};
const Product = require("../models/Product");
const auth = require("../auth");



// Adding products

module.exports.addProduct = (data) =>{
	console.log(data);

	if(data.isAdmin){
		let newProduct = new Product({
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
			});

		return newProduct.save().then((product,error)=>{
		if(error){
			return false;
		}else{
			return "Product successfully added.";
		}
	});
	}else{
		return false;
	}	
};


// Retrieving all products

module.exports.getAllProducts = () =>{
	return Product.find({}).then(result=>{
		return result;
	})
};


// Retrieving all active products

module.exports.getAllActive = () =>{
	return Product.find({isActive:true}).then(result=>{
		return result;
	})
};



// Retrive a single product

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result=>{
		return result;
	})
};


// Update product information

module.exports.updateProduct = (reqParams, reqBody)=>{
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((product,error)=>{
			if(error){
				return false;
			}else{
				return "Product successfully updated.";
			};
		});
};

// Archiving a product

module.exports.archiveProduct = (reqParams, reqBody)=>{
		let archivedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};
		return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,error)=>{
			if(error){
				return false;
			}else{
				return "Product successfully archived.";
			};
		});
};


// Activating a product

// module.exports.activateProduct = (reqParams)=>{
// 	return Product.findByIdAndUpdate(reqParams,{isActive:true}).then((product,error)=>{
// 		if(error){
// 			return false
// 		}else{
// 			return true
// 		}
// 	});
// };


module.exports.activateProduct = (reqParams, reqBody)=>{
		let activatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			isActive: reqBody.isActive
		};
		return Product.findByIdAndUpdate(reqParams.productId,activatedProduct).then((product,error)=>{
			if(error){
				return false;
			}else{
				return "Product successfully activated.";
			};
		});
};

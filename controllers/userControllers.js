const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register user

module.exports.registerUser = (reqBody) =>{
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)

	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return "Thank you for registering!";
		}
	})
};


// User authentication

module.exports.loginUser = (reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}else{
				return false;
			}
		}
	})
};

// Get user details

module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result
	})
};


// Make user as admin

module.exports.updateUser = (reqParams, reqBody)=>{
		let updatedAdmin = {
			isAdmin: reqBody.isAdmin
		};
		return User.findByIdAndUpdate(reqParams.userId,updatedAdmin).then((user,error)=>{
			if(error){
				return false;
			}else{
				return "User is now admin.";
			};
		});
};

// Admin user details

module.exports.getAdmin = (reqParams) =>{
	return User.findById(reqParams.userId).then(result=>{
		return result;
	})
};


// All user details

module.exports.getUsers = (data) => {
	return User.findById(data.userId).then(result=>{
		result.password = "";
		return result;
	})
}



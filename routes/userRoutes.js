const express = require ("express");
const router = express.Router();


const userController = require("../controllers/userControllers");
const auth = require("../auth");




// Route to register a user

router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
});


//  Route for user authentication

router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
})


 // Route for retrieving user details

router.post("/details",(req,res)=>{
	userController.getProfile({userId:req.body.id}).then(resultFromController=>res.send(resultFromController))
});


// Make user as admin 

router.put("/:userId",(req,res)=>{
	userController.updateUser(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// Route for Checking admin user

router.get("/:userId",(req,res)=>{
	userController.getAdmin(req.params).then(resultFromController=>res.send(resultFromController))
});


// Route for retrieving all users

router.get("/details",auth.verify,(req,res) =>{

	const userData = auth.decode(req.headers.authorization);


	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController))
});



module.exports = router;



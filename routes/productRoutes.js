const express = require ("express");
const router = express.Router();

const productController = require("../controllers/productControllers");
const auth = require("../auth");



// Route for Adding products

router.post("/",auth.verify,(req,res)=>{
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))
});

// Route for retrieving all products

router.get("/all",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController))
});


// Route for retrieving all active courses

router.get("/",(req,res)=>{
	productController.getAllActive().then(resultFromController=>res.send(resultFromController));
});


//  Route retrieving a specific/single product

router.get("/:productId",(req,res)=>{
	productController.getProduct(req.params).then(resultFromController=>res.send(resultFromController))
});


// Route for updating product information

router.put("/:productId",auth.verify,(req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// Route for archiving a product

router.put("/:productId/archive",auth.verify,(req,res)=>{
	productController.archiveProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


// Route for activating a product

router.put("/:productId/activate",auth.verify,(req,res)=>{
	productController.activateProduct(req.params,req.body).then(resultFromController=>res.send(resultFromController))
});


module.exports = router;
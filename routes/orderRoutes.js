const express = require ("express");
const router = express.Router();

const orderController = require("../controllers/orderControllers");
const auth = require("../auth");


// Routes for creating an order

router.post("/",auth.verify,(req,res)=>{
	const data = {
		totalAmount: req.body.totalAmount,
		userData: auth.decode(req.headers.authorization).id,
		userId: req.body.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.body.productId,
		quantity: req.body.quantity
	};
	orderController.createOrder(data).then(resultFromController=>res.send(resultFromController))
});



//  Route retrieving a specific/single product

router.get("/:orderId",(req,res)=>{
	orderController.getOrder(req.params).then(resultFromController=>res.send(resultFromController))
});


// Route for retrieving orders

router.get("/",(req,res)=>{
	orderController.getAllOrder().then(resultFromController=>res.send(resultFromController))
});











module.exports = router;
